package kz.astana.widgetuse;

public class Item {
    public String text;
    public int resourceId;

    public Item(String text, int resourceId) {
        this.text = text;
        this.resourceId = resourceId;
    }
}
