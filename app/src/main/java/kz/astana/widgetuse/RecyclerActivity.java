package kz.astana.widgetuse;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        ArrayList<Item> items = new ListElements().getListItems();
        MyAdapter myAdapter = new MyAdapter(items);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(myAdapter);

        myAdapter.setListener(new MyAdapter.MyOnClickListener() {
            @Override
            public void onClick(Item item) {
                Snackbar
                        .make(
                                recyclerView,
                                item.text,
                                Snackbar.LENGTH_INDEFINITE
                        )
                        .setAction(
                                "Back",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
//                                        finish();
                                        onBackPressed();
                                    }
                                }
                        )
                        .show();
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RecyclerActivity.this, SpinnerActivity.class));
            }
        });
    }
}