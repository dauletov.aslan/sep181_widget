package kz.astana.widgetuse;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

public class SpinnerActivity extends AppCompatActivity {

    private Spinner spinner;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        linearLayout = findViewById(R.id.linearLayout);
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                SpinnerActivity.this,
                android.R.layout.simple_spinner_item,
                new ListElements().elements
        );
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Snackbar.make(linearLayout, position + "", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Snackbar.make(linearLayout, "Nothing", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    public void onButtonClick(View view) {
        String s = "Selected item: " + spinner.getSelectedItem().toString() +
                "\nPosition: " + spinner.getSelectedItemPosition();

        Snackbar.make(linearLayout, s, Snackbar.LENGTH_SHORT).show();
    }

    public void onGridClick(View view) {
        startActivity(new Intent(SpinnerActivity.this, GridActivity.class));
    }
}