package kz.astana.widgetuse;

import java.util.ArrayList;

public class ListElements {

    public String[] elements = {
            "Apple",
            "Pineapple",
            "Orange",
            "Cherry",
            "Banana",
            "Kiwi",
            "Limon",
            "Watermelon"
    };

    public ArrayList<Item> getListItems() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new Item("Apple", R.drawable.ic_anger));
        items.add(new Item("Pineapple", R.drawable.ic_depressed));
        items.add(new Item("Orange", R.drawable.ic_iceberg));
        items.add(new Item("Cherry", R.drawable.ic_heart));

        return items;
    }
}
