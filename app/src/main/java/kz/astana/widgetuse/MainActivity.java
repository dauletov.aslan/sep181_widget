package kz.astana.widgetuse;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);
        FloatingActionButton button = findViewById(R.id.fab);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                MainActivity.this,
//                android.R.layout.simple_list_item_multiple_choice,
                android.R.layout.simple_list_item_single_choice,
                new ListElements().elements
        );
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = listView.getCheckedItemPosition();
                String s = adapter.getItem(pos);
//                SparseBooleanArray a = listView.getCheckedItemPositions();
//                for (int i = 0; i < a.size(); i++) {
//                    Log.d("Hello", i + ": " + a.get(i));
//                }

                Snackbar
                        .make(
                                listView,
                                s,
                                Snackbar.LENGTH_INDEFINITE
                        )
                        .setAction(
                                "Recycler",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent i = new Intent(MainActivity.this, RecyclerActivity.class);
                                        startActivity(i);
                                    }
                                }
                        )
                        .show();
            }
        });
    }
}